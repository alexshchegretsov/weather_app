from django.contrib import admin
from .models import City
# Register your models here.

# admin.site.register(City)

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    # список отображаемых полей
    list_display = ["name", "created"]
    # фильтр по заданным полям
    list_filter = ["name" ,"created"]
    # поиск по заданным полям
    search_fields = ["name"]
    # кол-во записей на одной странице, разбивается пагинатором
    list_per_page = 25

