from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="home"),
    path('delete/<int:city_id>/', views.remove_city, name="remove_city_url"),
]
