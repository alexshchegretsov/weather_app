from django.db import models


class City(models.Model):
    name = models.CharField(max_length=50)
    created = models.DateTimeField(auto_now_add=True)

    # учитывая, что имя определено в admin.py, используя
    # list_display - __str__ переопределять опционально
    def __str__(self):
        return self.name

    class Meta:
        # название модели в единственном числе в админке
        verbose_name = "citty"
        # название модели во множественном числе в админке
        verbose_name_plural = "cities"

    # переопределили метод сохранения в базу,
    # теперь все города записываются в базу с заглавной буквы
    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.name = self.name.title()
        super(City, self).save()
