import requests
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from .models import City
from .forms import CityForm
from random import randint as rd

def index(request):
    # initialize error messages
    err_msg = ""
    message = ""
    message_class = ""

    if request.method == "POST":
        form = CityForm(request.POST)
        # if form is correct
        if form.is_valid():
            new_city = form.cleaned_data["name"]
            # if this city does not exists in DB
            if not City.objects.filter(name__iexact=new_city).exists():
                # check correct city name
                url = f"http://api.openweathermap.org/data/2.5/weather?q={new_city}&units=metric&appid={settings.OPEN_WEATHER_API_KEY}"
                r = requests.get(url).json()
                if r["cod"] == 200:
                    form.save()
                else:
                    err_msg = f"{new_city} does not exists in the world!"
            else:
                err_msg = f"{new_city.title()} already exists!"

        # describe errors danger|success
        if err_msg:
            message = err_msg
            message_class = "is-danger"
        else:
            message = "Added Successfully!"
            message_class = "is-success"
    # GET
    form = CityForm()
    weather_cities = []
    cities = City.objects.all()
    for city in cities:
        # paste city name and make request
        url = f"http://api.openweathermap.org/data/2.5/weather?q={city.name}&units=metric&appid={settings.OPEN_WEATHER_API_KEY}"
        r = requests.get(url).json()

        city_weather = {
            "city": city,
            "temperature": r["main"]["temp"],
            "description": r["weather"][0]["description"],
            "wind_speed": r["wind"]["speed"],
            "icon": r["weather"][0]["icon"],
        }
        weather_cities.append(city_weather)


    quotes_url = 'https://quotesweatherapi.herokuapp.com/api/v1/quotes/'
    quotes = requests.get(quotes_url).json()
    choice = rd(0, len(quotes)-1)


    context = {
        "weather_cities": weather_cities,
        "form": form,
        "message": message,
        "message_class": message_class,
        "quote_title": quotes[choice]["title"],
    }
    return render(request, "weather/weather.html", context)


def remove_city(request, city_id):
    get_object_or_404(City, id=city_id).delete()
    return redirect("home")
