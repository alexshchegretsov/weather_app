import os
from django.http import HttpResponse
from django.conf import settings
from django.contrib.staticfiles.views import serve
from django.shortcuts import render

def serve_docs(request, path):

    docs_path = os.path.join(settings.DOCS_DIR, path)
    if os.path.isdir(docs_path):
        path = os.path.join(path, 'index.html')

    path = os.path.join(settings.DOCS_STATIC_NAMESPACE, path)
    return serve(request, path, insecure=True)

# BASE_DIR = '/home/alexander/Desktop/dev/weather_app/src'
# DOCS_DIR = '/home/alexander/Desktop/dev/weather_app/src/docs/static/docs_build'
# DOCS_STATIC_NAMESPACE = 'docs_build'